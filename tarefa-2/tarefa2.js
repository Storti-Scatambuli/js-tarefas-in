function converte() {
    let fahrenheit = parseFloat(document.getElementById('fahrenheit').value)
    let celsius = 5 * (fahrenheit - 32) / 9
    let celsiuslabel = document.getElementById('celsius')
    celsiuslabel.innerHTML = celsius.toFixed(2) + " °C"
}