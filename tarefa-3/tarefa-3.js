let nota1 = parseFloat(prompt("Nota 1: "));
let nota2 = parseFloat(prompt('Nota 2: '));
let nota3 = parseFloat(prompt('Nota 3: '));

let mediaAritimetica = (nota1 + nota2 + nota3) / 3;

if (mediaAritimetica >= 6) {
    console.log("O aluno obteve média " + mediaAritimetica.toFixed(2) + " portanto está aprovado.")
}
else {
    console.log("O aluno obteve média " + mediaAritimetica.toFixed(2) + "portanto está reprovado")
}