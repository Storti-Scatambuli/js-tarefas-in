//Ex 001

let valoresPositivos = []
let contValoresPositivos = 0
for (let i = 0; i < 6; i++) {
    cont = i
    valor = parseInt(prompt('Valor ' + ++cont));
    if (valor > 0) {
        valoresPositivos.push(valor)
        contValoresPositivos++
    }
}

alert('Foram digitados ' + contValoresPositivos + ' Valores positivos, esses foram: ' + valoresPositivos)

//Ex 002

function calculaNum() {
    let numX = parseInt(prompt('Num X')) 
    let numY = parseInt(prompt('Num Y'))
    produtoXY = numX * numY
    alert(numX + 'X' + numY + '=' + produtoXY)
    let continuar = prompt('Deseja fazer mais um cálculo? (y/n')
    if (continuar == 'y') {
        calculaNum()
    }
}